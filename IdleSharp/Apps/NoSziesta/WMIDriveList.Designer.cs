﻿namespace IdleSharp.Apps.NoSziesta
{
	partial class WMIDriveList
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.driveListPanel = new System.Windows.Forms.Panel();
			this.emptyLabel = new System.Windows.Forms.Label();
			this.driveListPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// driveListPanel
			// 
			this.driveListPanel.Controls.Add(this.emptyLabel);
			this.driveListPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.driveListPanel.Location = new System.Drawing.Point(0, 0);
			this.driveListPanel.Name = "driveListPanel";
			this.driveListPanel.Size = new System.Drawing.Size(313, 147);
			this.driveListPanel.TabIndex = 2;
			this.driveListPanel.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.driveListPanel_ListChanged);
			this.driveListPanel.ControlRemoved += new System.Windows.Forms.ControlEventHandler(this.driveListPanel_ListChanged);
			// 
			// emptyLabel
			// 
			this.emptyLabel.Dock = System.Windows.Forms.DockStyle.Top;
			this.emptyLabel.Location = new System.Drawing.Point(0, 0);
			this.emptyLabel.Name = "emptyLabel";
			this.emptyLabel.Padding = new System.Windows.Forms.Padding(10);
			this.emptyLabel.Size = new System.Drawing.Size(313, 33);
			this.emptyLabel.TabIndex = 0;
			this.emptyLabel.Text = "List is empty";
			this.emptyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.emptyLabel.EnabledChanged += new System.EventHandler(this.emptyLabel_EnabledChanged);
			// 
			// WMIDriveList
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.driveListPanel);
			this.Name = "WMIDriveList";
			this.Size = new System.Drawing.Size(313, 147);
			this.driveListPanel.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel driveListPanel;
		private System.Windows.Forms.Label emptyLabel;
	}
}
