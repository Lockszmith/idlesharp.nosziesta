
[sz]|H:\_Projects\Code\@bitbucket\IdleSharp.NoSziesta		| Sun 04/01/2012 | 16:01:03.73
> @git.exe %*
﻿using System;
using System.Windows.Forms;
using IdleSharp.Sys;
using System.Linq;

namespace IdleSharp.Apps.NoSziesta
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			if (args.Contains("-?") || args.Contains("-h") || args.Contains("--help") || args.Contains("/?") || args.Contains("/h") || args.Contains("/help"))
			{
				var helpForm = new CliHelp();
				helpForm.ShowDialog();
				return;
			}
			if (args.Contains("-p") || args.Contains("--portable") || args.Contains("/p") || args.Contains("/portable"))
			{
				PortableSettingsProvider.MakePortable(Properties.Settings.Default);
				//PortableSettingsProvider.MakePortable(Properties.LastUsedSettings.Default);
				//PortableSettingsProvider.MakePortable(Properties.DefaultSettings.Default);
			}

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new MainForm());
		}
	}
}

[sz]|H:\_Projects\Code\@bitbucket\IdleSharp.NoSziesta		| Sun 04/01/2012 | 16:01:03.75
> @set ErrorLevel=%ErrorLevel%

[sz]|H:\_Projects\Code\@bitbucket\IdleSharp.NoSziesta		| Sun 04/01/2012 | 16:01:03.75
> @rem Restore the original console codepage.

[sz]|H:\_Projects\Code\@bitbucket\IdleSharp.NoSziesta		| Sun 04/01/2012 | 16:01:03.75
> @chcp %cp_oem% > nul < nul
