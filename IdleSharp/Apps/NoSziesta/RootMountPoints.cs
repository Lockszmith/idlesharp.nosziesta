﻿using System;
using System.Collections.Generic;
using System.IO;

namespace IdleSharp.Apps.NoSziesta
{
    public class DiskInfo
    {
        public WMIClasses.DiskDrive WMIDiskDrive { get; private set; }
        public List<string> MountPoints { get; private set; }
        //System.Collections.Specialized.StringCollection

        public DiskInfo( WMIClasses.DiskDrive WMIDiskDrive )
        {
            this.WMIDiskDrive = WMIDiskDrive;
            this.MountPoints = new List<string>();
            //this.MountPoints = new System.Collections.Specialized.StringCollection();
        }

        public class Collection: System.Collections.ObjectModel.KeyedCollection<string,DiskInfo>
        {
            protected override string GetKeyForItem(DiskInfo item)
            {
                return item.WMIDiskDrive.Key;
            }
        }

        public override string ToString()
        {
            return ToString(string.Empty);
        }

        public string ToString(string Format)
        {
            switch (Format)
            {
                case "S":
                case "Short":
                    Format = "{0} ({1})";
                    break;
                default:
                    Format = "{0} ({1}): {2}";
                    break;
            }
            return string.Format(Format, WMIDiskDrive.Key, WMIDiskDrive[WMIClasses.DiskDrive.PropertyName.InterfaceType], string.Join(", ", MountPoints.ToArray()));
        }
    }
    

    public class RootMountPoints
    {
        public DiskInfo.Collection DiskInfoColleciton { get; private set; }

        private RootMountPoints()
        {
            DiskInfoColleciton = new DiskInfo.Collection();
        }

        public static RootMountPoints GetSystemRootMountPoints()
        {
            var newRootMountPoints = new RootMountPoints();
            // WMI Class Relationships used: DiskDrive --> DiskPartition --> LogicalDisk --> Directory --> Volume --> MountPoint

            //var output = Console.Out;
            newRootMountPoints.DiskInfoColleciton.Clear();

            //var colA = WMIClass.Generic.GetInstances(WMIClass.ClassName.Win32_DiskDrive);
            var colA = new WMIClass.Factory<WMIClasses.DiskDrive>().GetInstances();
            foreach (var a in colA)
            {
                var newDiskInfo = new DiskInfo(a);
                //PrintDebugInfo(output, a); outputWriteLine();
                var colB = new WMIClass.Factory<WMIClasses.DiskPartition>().GetInstancesRelatedTo(a);
                foreach (var b in colB)
                {
                    //outputWrite("{0} --> ", a.WMIClassName );
                    //PrintDebugInfo(output, b); outputWriteLine();
                    var colC = new WMIClass.Factory<WMIClasses.LogicalDisk>().GetInstancesRelatedTo(b);
                    foreach (var c in colC)
                    {
                        //outputWrite("{0} --> {1} --> ", a.WMIClassName, b.WMIClassName);
                        //PrintDebugInfo(output, c); outputWriteLine();
                        var colD = new WMIClass.Factory<WMIClasses.Directory>().GetInstancesRelatedTo(c);
                        foreach (var d in colD)
                        {
                            //outputWrite("{0} --> {1} --> {2} --> ", a.WMIClassName, b.WMIClassName, c.WMIClassName);
                            //PrintDebugInfo(output, d); outputWriteLine();
                            var colE = new WMIClass.Factory<WMIClasses.Volume>().GetInstancesRelatedTo(d);
                            foreach (var e in colE)
                            {
                                //outputWrite("{0} --> {1} --> {2} --> {3}", a.WMIClassName, b.WMIClassName, c.WMIClassName, d.WMIClassName);
                                //PrintDebugInfo(output, e); outputWriteLine();
                                var colF = new WMIClass.Factory<WMIClasses.MountPoint>().GetInstancesFromRelationshipTo(e);
                                foreach (var f in colF)
                                {
                                    //outputWrite("{0} --> {1} --> {2} --> {3} --> {4}", a.WMIClassName, b.WMIClassName, c.WMIClassName, d.WMIClassName, e.WMIClassName);
                                    //PrintDebugInfo(output, f); outputWriteLine();
                                    newDiskInfo.MountPoints.Add(f[WMIClasses.MountPoint.PropertyName.Directory].Replace(@"\\", @"\"));
                                }
                            }
                        }
                    }
                }
                newRootMountPoints.DiskInfoColleciton.Add(newDiskInfo);
            }

            //foreach (var m in DiskInfoColleciton)
            //{
            //    Console.WriteLine(m);
            //}
            return newRootMountPoints;
        }

        public bool GetDiskInfoFromPath( string Path, out DiskInfo result )
        {
            result = null;
            var resultMP = string.Empty;

            Path = Path.Replace(@"\\",@"\");

            foreach (var di in DiskInfoColleciton)
            {
                foreach (var mp in di.MountPoints)
                {
                    if (Path.StartsWith(mp, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (mp.StartsWith(resultMP, StringComparison.InvariantCultureIgnoreCase))
                        {
                            result = di;
                            resultMP = mp;
                        }
                    }
                }
            }

            return result != null;
        }

        #region PrintDebugInfo method
        private static void PrintDebugInfo(TextWriter textWriter, WMIClass a)
        {
            textWriter.WriteLine("{0} = [{1}])", a.WMIClassName, a.Key);
            using (var relA = a.wmiObject.GetRelated())
            {
                textWriter.WriteLine("Related", a.WMIClassName, a.Key);
                foreach (var rel in relA)
                {
                    textWriter.WriteLine("\t{0}", rel.ClassPath);
                }
            }
            using (var relA = a.wmiObject.GetRelationships())
            {
                textWriter.WriteLine("Relationships", a.WMIClassName, a.Key);
                foreach (var rel in relA)
                {
                    textWriter.WriteLine("\t{0}", rel.ClassPath);
                }
            }
            textWriter.WriteLine("---------");
            foreach (var p in a.Properties.Keys)
            {
                textWriter.WriteLine("\t{0}={1};", p, a.Properties[p.ToString()]);
            }
        }
        #endregion
    }
}
