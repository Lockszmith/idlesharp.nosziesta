
[sz]|H:\_Projects\Code\@bitbucket\IdleSharp.NoSziesta		| Sun 04/01/2012 | 16:00:49.03
> @git.exe %*
﻿namespace IdleSharp.Apps.NoSziesta
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
			this.busyToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.wmiDriveList = new IdleSharp.Apps.NoSziesta.WMIDriveList();
			this.panel2 = new System.Windows.Forms.Panel();
			this.autoDetectAndActivateAllUSBDrivesCheckBox = new System.Windows.Forms.CheckBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.alertPictureBox = new System.Windows.Forms.PictureBox();
			this.startMinimizedCheckBox = new System.Windows.Forms.CheckBox();
			this.Sequential = new System.Windows.Forms.ComboBox();
			this.writeSizeLabel = new System.Windows.Forms.Label();
			this.driveModeLabel = new System.Windows.Forms.Label();
			this.writeSizeKBLabel = new System.Windows.Forms.Label();
			this.regularIntervalSecondsLabel = new System.Windows.Forms.Label();
			this.randomSecondsLabel = new System.Windows.Forms.Label();
			this.disableRadioButton = new System.Windows.Forms.RadioButton();
			this.writeSizeNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.regularIntervalNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.randomIntervalRadioButton = new System.Windows.Forms.RadioButton();
			this.randomMaxLabel = new System.Windows.Forms.Label();
			this.randomMinNmericUpDown = new System.Windows.Forms.NumericUpDown();
			this.regularIntervalRadioButton = new System.Windows.Forms.RadioButton();
			this.randomMinLabel = new System.Windows.Forms.Label();
			this.randomMaxNmericUpDown = new System.Windows.Forms.NumericUpDown();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.driveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.addNewDriveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.addAllUsbToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.clearDrivesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.addNewDriveToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.addAllUsbToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.clearDrivesToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
			this.notifyIconContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.showNotifyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.exitNotifyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
			this.toolStripContainer1.ContentPanel.SuspendLayout();
			this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
			this.toolStripContainer1.SuspendLayout();
			this.statusStrip1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.alertPictureBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.writeSizeNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.regularIntervalNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.randomMinNmericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.randomMaxNmericUpDown)).BeginInit();
			this.menuStrip1.SuspendLayout();
			this.toolStrip1.SuspendLayout();
			this.notifyIconContextMenuStrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// toolStripContainer1
			// 
			// 
			// toolStripContainer1.BottomToolStripPanel
			// 
			this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip1);
			// 
			// toolStripContainer1.ContentPanel
			// 
			this.toolStripContainer1.ContentPanel.Controls.Add(this.wmiDriveList);
			this.toolStripContainer1.ContentPanel.Controls.Add(this.panel2);
			this.toolStripContainer1.ContentPanel.Controls.Add(this.panel1);
			this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(370, 380);
			this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
			this.toolStripContainer1.Name = "toolStripContainer1";
			this.toolStripContainer1.Size = new System.Drawing.Size(370, 451);
			this.toolStripContainer1.TabIndex = 0;
			this.toolStripContainer1.Text = "toolStripContainer1";
			// 
			// toolStripContainer1.TopToolStripPanel
			// 
			this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.menuStrip1);
			this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
			// 
			// statusStrip1
			// 
			this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.busyToolStripStatusLabel});
			this.statusStrip1.Location = new System.Drawing.Point(0, 0);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(370, 22);
			this.statusStrip1.TabIndex = 2;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// toolStripStatusLabel1
			// 
			this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
			this.toolStripStatusLabel1.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
			this.toolStripStatusLabel1.Size = new System.Drawing.Size(316, 17);
			this.toolStripStatusLabel1.Spring = true;
			// 
			// busyToolStripStatusLabel
			// 
			this.busyToolStripStatusLabel.Name = "busyToolStripStatusLabel";
			this.busyToolStripStatusLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
			this.busyToolStripStatusLabel.Size = new System.Drawing.Size(39, 17);
			this.busyToolStripStatusLabel.Text = "Ready";
			this.busyToolStripStatusLabel.ToolTipText = "Writing...";
			this.busyToolStripStatusLabel.EnabledChanged += new System.EventHandler(this.busyToolStripStatusLabel_EnabledChanged);
			// 
			// wmiDriveList
			// 
			this.wmiDriveList.Dock = System.Windows.Forms.DockStyle.Fill;
			this.wmiDriveList.Location = new System.Drawing.Point(0, 25);
			this.wmiDriveList.Name = "wmiDriveList";
			this.wmiDriveList.Size = new System.Drawing.Size(370, 196);
			this.wmiDriveList.TabIndex = 2;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.autoDetectAndActivateAllUSBDrivesCheckBox);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(370, 25);
			this.panel2.TabIndex = 1;
			// 
			// autoDetectAndActivateAllUSBDrivesCheckBox
			// 
			this.autoDetectAndActivateAllUSBDrivesCheckBox.AutoSize = true;
			this.autoDetectAndActivateAllUSBDrivesCheckBox.Checked = global::IdleSharp.Apps.NoSziesta.Properties.Settings.Default.AutoLoadUSBDrives;
			this.autoDetectAndActivateAllUSBDrivesCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::IdleSharp.Apps.NoSziesta.Properties.Settings.Default, "AutoLoadUSBDrives", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.autoDetectAndActivateAllUSBDrivesCheckBox.Location = new System.Drawing.Point(12, 5);
			this.autoDetectAndActivateAllUSBDrivesCheckBox.Name = "autoDetectAndActivateAllUSBDrivesCheckBox";
			this.autoDetectAndActivateAllUSBDrivesCheckBox.Size = new System.Drawing.Size(212, 17);
			this.autoDetectAndActivateAllUSBDrivesCheckBox.TabIndex = 0;
			this.autoDetectAndActivateAllUSBDrivesCheckBox.Text = "Auto detect and activate all USB drives";
			this.autoDetectAndActivateAllUSBDrivesCheckBox.UseVisualStyleBackColor = true;
			this.autoDetectAndActivateAllUSBDrivesCheckBox.CheckedChanged += new System.EventHandler(this.autoDetectAndActivateAllUSBDrivesCheckBox_CheckedChanged);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.alertPictureBox);
			this.panel1.Controls.Add(this.startMinimizedCheckBox);
			this.panel1.Controls.Add(this.Sequential);
			this.panel1.Controls.Add(this.writeSizeLabel);
			this.panel1.Controls.Add(this.driveModeLabel);
			this.panel1.Controls.Add(this.writeSizeKBLabel);
			this.panel1.Controls.Add(this.regularIntervalSecondsLabel);
			this.panel1.Controls.Add(this.randomSecondsLabel);
			this.panel1.Controls.Add(this.disableRadioButton);
			this.panel1.Controls.Add(this.writeSizeNumericUpDown);
			this.panel1.Controls.Add(this.regularIntervalNumericUpDown);
			this.panel1.Controls.Add(this.randomIntervalRadioButton);
			this.panel1.Controls.Add(this.randomMaxLabel);
			this.panel1.Controls.Add(this.randomMinNmericUpDown);
			this.panel1.Controls.Add(this.regularIntervalRadioButton);
			this.panel1.Controls.Add(this.randomMinLabel);
			this.panel1.Controls.Add(this.randomMaxNmericUpDown);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 221);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(370, 159);
			this.panel1.TabIndex = 0;
			// 
			// alertPictureBox
			// 
			this.alertPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.alertPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.alertPictureBox.Location = new System.Drawing.Point(348, 7);
			this.alertPictureBox.Name = "alertPictureBox";
			this.alertPictureBox.Size = new System.Drawing.Size(16, 16);
			this.alertPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.alertPictureBox.TabIndex = 15;
			this.alertPictureBox.TabStop = false;
			this.alertPictureBox.Visible = false;
			// 
			// startMinimizedCheckBox
			// 
			this.startMinimizedCheckBox.AutoSize = true;
			this.startMinimizedCheckBox.Checked = global::IdleSharp.Apps.NoSziesta.Properties.Settings.Default.StartMinimized;
			this.startMinimizedCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::IdleSharp.Apps.NoSziesta.Properties.Settings.Default, "StartMinimized", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.startMinimizedCheckBox.Location = new System.Drawing.Point(12, 135);
			this.startMinimizedCheckBox.Name = "startMinimizedCheckBox";
			this.startMinimizedCheckBox.Size = new System.Drawing.Size(96, 17);
			this.startMinimizedCheckBox.TabIndex = 14;
			this.startMinimizedCheckBox.Text = "Start minimized";
			this.startMinimizedCheckBox.UseVisualStyleBackColor = true;
			// 
			// Sequential
			// 
			this.Sequential.DataBindings.Add(new System.Windows.Forms.Binding("Name", global::IdleSharp.Apps.NoSziesta.Properties.Settings.Default, "MultiDriveMode", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.Sequential.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.Sequential.FormattingEnabled = true;
			this.Sequential.Location = new System.Drawing.Point(147, 107);
			this.Sequential.Name = global::IdleSharp.Apps.NoSziesta.Properties.Settings.Default.MultiDriveMode;
			this.Sequential.Size = new System.Drawing.Size(176, 21);
			this.Sequential.TabIndex = 13;
			this.Sequential.Visible = false;
			// 
			// writeSizeLabel
			// 
			this.writeSizeLabel.AutoSize = true;
			this.writeSizeLabel.Location = new System.Drawing.Point(30, 83);
			this.writeSizeLabel.Name = "writeSizeLabel";
			this.writeSizeLabel.Size = new System.Drawing.Size(71, 13);
			this.writeSizeLabel.TabIndex = 12;
			this.writeSizeLabel.Text = "Temp file size";
			// 
			// driveModeLabel
			// 
			this.driveModeLabel.AutoSize = true;
			this.driveModeLabel.Location = new System.Drawing.Point(30, 110);
			this.driveModeLabel.Name = "driveModeLabel";
			this.driveModeLabel.Size = new System.Drawing.Size(84, 13);
			this.driveModeLabel.TabIndex = 12;
			this.driveModeLabel.Text = "Multi-drive mode";
			this.driveModeLabel.Visible = false;
			// 
			// writeSizeKBLabel
			// 
			this.writeSizeKBLabel.AutoSize = true;
			this.writeSizeKBLabel.Location = new System.Drawing.Point(218, 83);
			this.writeSizeKBLabel.Name = "writeSizeKBLabel";
			this.writeSizeKBLabel.Size = new System.Drawing.Size(21, 13);
			this.writeSizeKBLabel.TabIndex = 5;
			this.writeSizeKBLabel.Text = "KB";
			// 
			// regularIntervalSecondsLabel
			// 
			this.regularIntervalSecondsLabel.AutoSize = true;
			this.regularIntervalSecondsLabel.Location = new System.Drawing.Point(196, 57);
			this.regularIntervalSecondsLabel.Name = "regularIntervalSecondsLabel";
			this.regularIntervalSecondsLabel.Size = new System.Drawing.Size(47, 13);
			this.regularIntervalSecondsLabel.TabIndex = 5;
			this.regularIntervalSecondsLabel.Text = "seconds";
			// 
			// randomSecondsLabel
			// 
			this.randomSecondsLabel.AutoSize = true;
			this.randomSecondsLabel.Location = new System.Drawing.Point(276, 31);
			this.randomSecondsLabel.Name = "randomSecondsLabel";
			this.randomSecondsLabel.Size = new System.Drawing.Size(47, 13);
			this.randomSecondsLabel.TabIndex = 10;
			this.randomSecondsLabel.Text = "seconds";
			// 
			// disableRadioButton
			// 
			this.disableRadioButton.AutoSize = true;
			this.disableRadioButton.Checked = true;
			this.disableRadioButton.Location = new System.Drawing.Point(12, 6);
			this.disableRadioButton.Name = "disableRadioButton";
			this.disableRadioButton.Size = new System.Drawing.Size(85, 17);
			this.disableRadioButton.TabIndex = 0;
			this.disableRadioButton.TabStop = true;
			this.disableRadioButton.Text = "Disable timer";
			this.toolTip1.SetToolTip(this.disableRadioButton, "Disable timer");
			this.disableRadioButton.UseVisualStyleBackColor = true;
			this.disableRadioButton.CheckedChanged += new System.EventHandler(this.timerSettings_RadioButtonChanged);
			// 
			// writeSizeNumericUpDown
			// 
			this.writeSizeNumericUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::IdleSharp.Apps.NoSziesta.Properties.Settings.Default, "TempFileSize", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.writeSizeNumericUpDown.Increment = new decimal(new int[] {
            64,
            0,
            0,
            0});
			this.writeSizeNumericUpDown.Location = new System.Drawing.Point(147, 81);
			this.writeSizeNumericUpDown.Maximum = new decimal(new int[] {
            16384,
            0,
            0,
            0});
			this.writeSizeNumericUpDown.Name = "writeSizeNumericUpDown";
			this.writeSizeNumericUpDown.Size = new System.Drawing.Size(65, 20);
			this.writeSizeNumericUpDown.TabIndex = 1;
			this.writeSizeNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.writeSizeNumericUpDown.Value = global::IdleSharp.Apps.NoSziesta.Properties.Settings.Default.TempFileSize;
			this.writeSizeNumericUpDown.ValueChanged += new System.EventHandler(this.timerSettings_ValueChanged);
			// 
			// regularIntervalNumericUpDown
			// 
			this.regularIntervalNumericUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::IdleSharp.Apps.NoSziesta.Properties.Settings.Default, "RegularInterval", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.regularIntervalNumericUpDown.Location = new System.Drawing.Point(147, 55);
			this.regularIntervalNumericUpDown.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
			this.regularIntervalNumericUpDown.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
			this.regularIntervalNumericUpDown.Name = "regularIntervalNumericUpDown";
			this.regularIntervalNumericUpDown.Size = new System.Drawing.Size(43, 20);
			this.regularIntervalNumericUpDown.TabIndex = 1;
			this.regularIntervalNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.regularIntervalNumericUpDown.Value = global::IdleSharp.Apps.NoSziesta.Properties.Settings.Default.RegularInterval;
			this.regularIntervalNumericUpDown.ValueChanged += new System.EventHandler(this.timerSettings_ValueChanged);
			// 
			// randomIntervalRadioButton
			// 
			this.randomIntervalRadioButton.AutoSize = true;
			this.randomIntervalRadioButton.Location = new System.Drawing.Point(12, 29);
			this.randomIntervalRadioButton.Name = "randomIntervalRadioButton";
			this.randomIntervalRadioButton.Size = new System.Drawing.Size(102, 17);
			this.randomIntervalRadioButton.TabIndex = 6;
			this.randomIntervalRadioButton.Text = "Random interval";
			this.randomIntervalRadioButton.UseVisualStyleBackColor = true;
			this.randomIntervalRadioButton.CheckedChanged += new System.EventHandler(this.timerSettings_RadioButtonChanged);
			// 
			// randomMaxLabel
			// 
			this.randomMaxLabel.AutoSize = true;
			this.randomMaxLabel.Location = new System.Drawing.Point(197, 31);
			this.randomMaxLabel.Name = "randomMaxLabel";
			this.randomMaxLabel.Size = new System.Drawing.Size(26, 13);
			this.randomMaxLabel.TabIndex = 11;
			this.randomMaxLabel.Text = "max";
			this.randomMaxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// randomMinNmericUpDown
			// 
			this.randomMinNmericUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::IdleSharp.Apps.NoSziesta.Properties.Settings.Default, "RandomIntervalMin", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.randomMinNmericUpDown.Location = new System.Drawing.Point(147, 29);
			this.randomMinNmericUpDown.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
			this.randomMinNmericUpDown.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
			this.randomMinNmericUpDown.Name = "randomMinNmericUpDown";
			this.randomMinNmericUpDown.Size = new System.Drawing.Size(43, 20);
			this.randomMinNmericUpDown.TabIndex = 7;
			this.randomMinNmericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.randomMinNmericUpDown.Value = global::IdleSharp.Apps.NoSziesta.Properties.Settings.Default.RandomIntervalMin;
			this.randomMinNmericUpDown.ValueChanged += new System.EventHandler(this.timerSettings_ValueChanged);
			// 
			// regularIntervalRadioButton
			// 
			this.regularIntervalRadioButton.AutoSize = true;
			this.regularIntervalRadioButton.Location = new System.Drawing.Point(12, 55);
			this.regularIntervalRadioButton.Name = "regularIntervalRadioButton";
			this.regularIntervalRadioButton.Size = new System.Drawing.Size(99, 17);
			this.regularIntervalRadioButton.TabIndex = 0;
			this.regularIntervalRadioButton.Text = "Regular interval";
			this.regularIntervalRadioButton.UseVisualStyleBackColor = true;
			this.regularIntervalRadioButton.CheckedChanged += new System.EventHandler(this.timerSettings_RadioButtonChanged);
			// 
			// randomMinLabel
			// 
			this.randomMinLabel.AutoSize = true;
			this.randomMinLabel.Location = new System.Drawing.Point(117, 31);
			this.randomMinLabel.Name = "randomMinLabel";
			this.randomMinLabel.Size = new System.Drawing.Size(23, 13);
			this.randomMinLabel.TabIndex = 8;
			this.randomMinLabel.Text = "min";
			this.randomMinLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// randomMaxNmericUpDown
			// 
			this.randomMaxNmericUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::IdleSharp.Apps.NoSziesta.Properties.Settings.Default, "RandomIntervalMax", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.randomMaxNmericUpDown.Location = new System.Drawing.Point(227, 29);
			this.randomMaxNmericUpDown.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
			this.randomMaxNmericUpDown.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
			this.randomMaxNmericUpDown.Name = "randomMaxNmericUpDown";
			this.randomMaxNmericUpDown.Size = new System.Drawing.Size(43, 20);
			this.randomMaxNmericUpDown.TabIndex = 9;
			this.randomMaxNmericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.randomMaxNmericUpDown.Value = global::IdleSharp.Apps.NoSziesta.Properties.Settings.Default.RandomIntervalMax;
			this.randomMaxNmericUpDown.ValueChanged += new System.EventHandler(this.timerSettings_ValueChanged);
			// 
			// menuStrip1
			// 
			this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
			this.menuStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.driveToolStripMenuItem,
            this.helpToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(370, 24);
			this.menuStrip1.TabIndex = 1;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "&File";
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
			this.exitToolStripMenuItem.Text = "E&xit";
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
			// 
			// driveToolStripMenuItem
			// 
			this.driveToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewDriveToolStripMenuItem,
            this.addAllUsbToolStripMenuItem,
            this.toolStripSeparator1,
            this.clearDrivesToolStripMenuItem});
			this.driveToolStripMenuItem.Name = "driveToolStripMenuItem";
			this.driveToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
			this.driveToolStripMenuItem.Text = "Drive";
			// 
			// addNewDriveToolStripMenuItem
			// 
			this.addNewDriveToolStripMenuItem.Name = "addNewDriveToolStripMenuItem";
			this.addNewDriveToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
			this.addNewDriveToolStripMenuItem.Text = "Add drive to list";
			this.addNewDriveToolStripMenuItem.Click += new System.EventHandler(this.addNewDriveToolStripMenuItem_Click);
			// 
			// addAllUsbToolStripMenuItem
			// 
			this.addAllUsbToolStripMenuItem.Name = "addAllUsbToolStripMenuItem";
			this.addAllUsbToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
			this.addAllUsbToolStripMenuItem.Text = "Auto-add all USB drives";
			this.addAllUsbToolStripMenuItem.Click += new System.EventHandler(this.addAllUsb_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(195, 6);
			// 
			// clearDrivesToolStripMenuItem
			// 
			this.clearDrivesToolStripMenuItem.Name = "clearDrivesToolStripMenuItem";
			this.clearDrivesToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
			this.clearDrivesToolStripMenuItem.Text = "Clear drive list";
			this.clearDrivesToolStripMenuItem.Click += new System.EventHandler(this.clearDrives_Click);
			// 
			// helpToolStripMenuItem
			// 
			this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
			this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
			this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
			this.helpToolStripMenuItem.Text = "&Help";
			// 
			// aboutToolStripMenuItem
			// 
			this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
			this.aboutToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
			this.aboutToolStripMenuItem.Text = "&About...";
			// 
			// toolStrip1
			// 
			this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewDriveToolStripButton,
            this.addAllUsbToolStripButton,
            this.toolStripSeparator2,
            this.clearDrivesToolStripButton});
			this.toolStrip1.Location = new System.Drawing.Point(3, 24);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(87, 25);
			this.toolStrip1.TabIndex = 0;
			// 
			// addNewDriveToolStripButton
			// 
			this.addNewDriveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.addNewDriveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("addNewDriveToolStripButton.Image")));
			this.addNewDriveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.addNewDriveToolStripButton.Name = "addNewDriveToolStripButton";
			this.addNewDriveToolStripButton.Size = new System.Drawing.Size(23, 22);
			this.addNewDriveToolStripButton.Text = "Add drive to list";
			this.addNewDriveToolStripButton.Click += new System.EventHandler(this.addNewDriveToolStripMenuItem_Click);
			// 
			// addAllUsbToolStripButton
			// 
			this.addAllUsbToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.addAllUsbToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("addAllUsbToolStripButton.Image")));
			this.addAllUsbToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.addAllUsbToolStripButton.Name = "addAllUsbToolStripButton";
			this.addAllUsbToolStripButton.Size = new System.Drawing.Size(23, 22);
			this.addAllUsbToolStripButton.Text = "Auto-add all USB drives";
			this.addAllUsbToolStripButton.Click += new System.EventHandler(this.addAllUsb_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
			// 
			// clearDrivesToolStripButton
			// 
			this.clearDrivesToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.clearDrivesToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("clearDrivesToolStripButton.Image")));
			this.clearDrivesToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.clearDrivesToolStripButton.Name = "clearDrivesToolStripButton";
			this.clearDrivesToolStripButton.Size = new System.Drawing.Size(23, 22);
			this.clearDrivesToolStripButton.Text = "Clear drive list";
			this.clearDrivesToolStripButton.Click += new System.EventHandler(this.clearDrives_Click);
			// 
			// timer1
			// 
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// notifyIcon1
			// 
			this.notifyIcon1.ContextMenuStrip = this.notifyIconContextMenuStrip;
			this.notifyIcon1.Visible = true;
			this.notifyIcon1.DoubleClick += new System.EventHandler(this.showNotifyToolStripMenuItem_Click);
			// 
			// notifyIconContextMenuStrip
			// 
			this.notifyIconContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showNotifyToolStripMenuItem,
            this.toolStripSeparator3,
            this.exitNotifyToolStripMenuItem});
			this.notifyIconContextMenuStrip.Name = "notifyIconContextMenuStrip";
			this.notifyIconContextMenuStrip.ShowImageMargin = false;
			this.notifyIconContextMenuStrip.Size = new System.Drawing.Size(81, 54);
			// 
			// showNotifyToolStripMenuItem
			// 
			this.showNotifyToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
			this.showNotifyToolStripMenuItem.Name = "showNotifyToolStripMenuItem";
			this.showNotifyToolStripMenuItem.Size = new System.Drawing.Size(80, 22);
			this.showNotifyToolStripMenuItem.Text = "Show";
			this.showNotifyToolStripMenuItem.Click += new System.EventHandler(this.showNotifyToolStripMenuItem_Click);
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(77, 6);
			// 
			// exitNotifyToolStripMenuItem
			// 
			this.exitNotifyToolStripMenuItem.Name = "exitNotifyToolStripMenuItem";
			this.exitNotifyToolStripMenuItem.Size = new System.Drawing.Size(80, 22);
			this.exitNotifyToolStripMenuItem.Text = "Exit";
			this.exitNotifyToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
			// 
			// toolTip1
			// 
			this.toolTip1.AutoPopDelay = 15000;
			this.toolTip1.InitialDelay = 500;
			this.toolTip1.ReshowDelay = 100;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(370, 451);
			this.Controls.Add(this.toolStripContainer1);
			this.MainMenuStrip = this.menuStrip1;
			this.MinimumSize = new System.Drawing.Size(386, 420);
			this.Name = "MainForm";
			this.Text = "NoSziesta by IdleSharp";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.Shown += new System.EventHandler(this.MainForm_Shown);
			this.Resize += new System.EventHandler(this.MainForm_Resize);
			this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
			this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
			this.toolStripContainer1.ContentPanel.ResumeLayout(false);
			this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
			this.toolStripContainer1.TopToolStripPanel.PerformLayout();
			this.toolStripContainer1.ResumeLayout(false);
			this.toolStripContainer1.PerformLayout();
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.alertPictureBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.writeSizeNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.regularIntervalNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.randomMinNmericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.randomMaxNmericUpDown)).EndInit();
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.notifyIconContextMenuStrip.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.RadioButton disableRadioButton;
        private System.Windows.Forms.Label regularIntervalSecondsLabel;
        private System.Windows.Forms.NumericUpDown regularIntervalNumericUpDown;
		private System.Windows.Forms.RadioButton regularIntervalRadioButton;
        private System.Windows.Forms.Label randomSecondsLabel;
        private System.Windows.Forms.RadioButton randomIntervalRadioButton;
        private System.Windows.Forms.Label randomMaxLabel;
        private System.Windows.Forms.NumericUpDown randomMinNmericUpDown;
        private System.Windows.Forms.Label randomMinLabel;
        private System.Windows.Forms.NumericUpDown randomMaxNmericUpDown;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem driveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNewDriveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearDrivesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton addNewDriveToolStripButton;
        private System.Windows.Forms.ToolStripButton clearDrivesToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ComboBox Sequential;
        private System.Windows.Forms.Label driveModeLabel;
        private System.Windows.Forms.CheckBox startMinimizedCheckBox;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel busyToolStripStatusLabel;
        private System.Windows.Forms.ContextMenuStrip notifyIconContextMenuStrip;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripButton addAllUsbToolStripButton;
        private System.Windows.Forms.PictureBox alertPictureBox;
        private System.Windows.Forms.ToolStripMenuItem showNotifyToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem exitNotifyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addAllUsbToolStripMenuItem;
        private System.Windows.Forms.Label writeSizeKBLabel;
        private System.Windows.Forms.NumericUpDown writeSizeNumericUpDown;
        private System.Windows.Forms.Label writeSizeLabel;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.CheckBox autoDetectAndActivateAllUSBDrivesCheckBox;
		private WMIDriveList wmiDriveList;
    }
}
[sz]|H:\_Projects\Code\@bitbucket\IdleSharp.NoSziesta		| Sun 04/01/2012 | 16:00:49.05
> @set ErrorLevel=%ErrorLevel%

[sz]|H:\_Projects\Code\@bitbucket\IdleSharp.NoSziesta		| Sun 04/01/2012 | 16:00:49.05
> @rem Restore the original console codepage.

[sz]|H:\_Projects\Code\@bitbucket\IdleSharp.NoSziesta		| Sun 04/01/2012 | 16:00:49.05
> @chcp %cp_oem% > nul < nul
