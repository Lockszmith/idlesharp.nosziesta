﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using IdleSharp.Sys;
using System.IO;

namespace IdleSharp.Apps.NoSziesta
{
	public partial class WMIDriveList : UserControl
	{
		public WMIDriveList()
		{
			InitializeComponent();
			systemRootMountPoints = RootMountPoints.GetSystemRootMountPoints();
		}

		int _count;
		int _busyCount;
		public int DriveCount {
			get { return _count; }
			private set
			{
				if (_count == value) return;
				bool activeStateChanged = (0 == value || 0 == _count);
				_count = value;
				if (activeStateChanged) this.TriggerEvent(ActiveStateChanged);
			}
		}
		public bool IsActive { get { return _count > 0; } }
		public bool IsBusy { get { return _busyCount > 0; } }

		public event EventHandler<UnhandledExceptionEventArgs> UnhandledException;
		public event EventHandler ActiveStateChanged;
		public event EventHandler BusyStateChanged;

		RootMountPoints systemRootMountPoints;

		void noSziestaDriveInfoUC1_RemoveClicked(object sender, EventArgs e)
		{
			var senderControl = sender as Control;
			this.driveListPanel.Controls.Remove(senderControl);
		}
		void noSziestaDriveInfoUC1_ActivatedStateChanged(object sender, EventArgs e)
		{
			RescanActiveDrives();
		}
		public void RescanActiveDrives()
		{
			int count = 0;
			foreach (var szDrive in AllDrives())
			{
				if (szDrive.Activated)
				{
					++count;
					break;
				}
			}
			DriveCount = count;

			var isActive = (count > 0);

			if (!isActive)
			{
				this.TriggerEvent(UnhandledException,
					new UnhandledExceptionEventArgs(
						new InvalidOperationException(
							"At least one drive must be valid and active to activate timer"
						)
					, false
					)
				);
			}
		}
		void noSziestaDriveInfoUC1_BusyStateChanged(object sender, EventArgs e)
		{
			var sz = sender as NoSziestaDriveInfoUC;
			bool isBusy = IsBusy;
			_busyCount += sz.IsBusy ? +1 : -1;

			if (isBusy != IsBusy)
			{
				this.TriggerEvent(BusyStateChanged);
			}
		}

		public IEnumerable<NoSziestaDriveInfoUC> AllDrives()
		{
			foreach (var c in driveListPanel.Controls)
			{
				var szDrive = c as NoSziestaDriveInfoUC;
				if (null != szDrive)
				{
					yield return szDrive;
				}
			}
		}

		public NoSziestaDriveInfoUC AddNewDrive(EntryType EntryType = null, string value = null)
		{
			NoSziestaDriveInfoUC newControl = new IdleSharp.Apps.NoSziesta.NoSziestaDriveInfoUC();
			newControl.UpdateRootMountPoints(systemRootMountPoints.DiskInfoColleciton);

			newControl.Dock = DockStyle.Top;
			newControl.RemoveClicked += new EventHandler(noSziestaDriveInfoUC1_RemoveClicked);
			newControl.ActivatedStateChanged += new EventHandler(noSziestaDriveInfoUC1_ActivatedStateChanged);
			newControl.BusyStateChanged += new EventHandler(noSziestaDriveInfoUC1_BusyStateChanged);
			this.driveListPanel.Controls.Add(newControl);
			newControl.BringToFront();

			if (null != EntryType)
			{
				newControl.Select(EntryType, value);
			}

			return newControl;
		}

		public void AutoAddAllUsbDrives( bool resetList = false, bool interactive = true )
		{
			if (resetList) Clear();

			foreach (var di in systemRootMountPoints.DiskInfoColleciton)
			{
				if ("USB" == di.WMIDiskDrive[WMIClasses.DiskDrive.PropertyName.InterfaceType] && di.MountPoints.Count > 0)
				{
					if (!File.Exists(Path.Combine(di.MountPoints[0], ".NoSziesta.NoAuto")))
					{
						if (!Directory.Exists(Path.Combine(di.MountPoints[0], ".NoSziesta")))
						{
							// Ask if to auto add, since this has not been used yet
							if (!interactive || DialogResult.No == MessageBox.Show(string.Format(
								"A drive that was never touched by NoSziesta has been detected{0}{0}Mount point(s):{0}\t{1}{0}{0}Add it [Yes], or skip [No]?"
								, Environment.NewLine
								, string.Join( Environment.NewLine + "\t", di.MountPoints )
								), "Auto add?", MessageBoxButtons.YesNo))
							{
								continue;
							}
						}
						AddNewDrive(EntryType.WMIDrive, di.WMIDiskDrive.Key).Activated = true;
					}
				}
			}
		}

		private void driveListPanel_ListChanged(object sender, ControlEventArgs e)
		{
			emptyLabel.Enabled = driveListPanel.Controls.Count == 1;
			RescanActiveDrives();
		}

		private void emptyLabel_EnabledChanged(object sender, EventArgs e)
		{
			emptyLabel.Visible = emptyLabel.Enabled;
		}

		public void Clear()
		{
			while (driveListPanel.Controls.Count > 1)
			{
				driveListPanel.Controls.RemoveAt((driveListPanel.Controls[0] is Label) ? 1 : 0);
			}
		}
	}
}
