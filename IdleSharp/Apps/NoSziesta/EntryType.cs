﻿using System;
using System.Collections.Generic;
using IdleSharp.Sys;

namespace IdleSharp.Apps.NoSziesta
{
	public class EntryType: EnumBase<EntryType>
	{
		public static readonly EntryType WMIDrive = new EntryType("WMIDrive");
		public static readonly EntryType CustomPath = new EntryType("CustomPath");

		private EntryType(String name) : base(name) { }
		public new static IEnumerable<EntryType> All { get { return EnumBase<EntryType>.All; } }
		public static explicit operator EntryType(string str) { return Parse(str); }
	}
}
