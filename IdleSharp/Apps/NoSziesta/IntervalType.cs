﻿using System;
using System.Collections.Generic;
using IdleSharp.Sys;

namespace IdleSharp.Apps.NoSziesta
{
	public class IntervalType: EnumBase<IntervalType, int>
	{
		public static readonly IntervalType Disabled = new IntervalType("Disabled", 0);
		public static readonly IntervalType Regular = new IntervalType("Regular", 1);
		public static readonly IntervalType Random = new IntervalType("Random", 3);

		private IntervalType(String name, int value) : base(name, value) { }
		public new static IEnumerable<IntervalType> All { get { return EnumBase<IntervalType, int>.All; } }
		public static explicit operator IntervalType(string str) { return Parse(str); }
		public static implicit operator int(IntervalType enumVal) { return enumVal.Value; }
	}
}
