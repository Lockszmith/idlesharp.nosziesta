
[sz]|H:\_Projects\Code\@bitbucket\IdleSharp.NoSziesta		| Sun 04/01/2012 | 16:01:00.86
> @git.exe %*
﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace IdleSharp.Apps.NoSziesta
{
    public partial class NoSziestaDriveInfoUC : UserControl
    {
        private const string customLocationComboBoxItem = "Custom Location...";
        public DiskInfo.Collection RootMountPoints { get; private set; }
        private int customIndex = -2;

        private bool prevActivated;
        public bool Activated { get { return activatedCheckBox.Checked; } set { activatedCheckBox.Checked = value; } }

        private bool prevBusy;
        public bool IsBusy { get { return activatedCheckBox.CheckState == CheckState.Indeterminate; } }

        public Guid RunTag { get; private set; }

        public event EventHandler ActivatedStateChanged;
        public event EventHandler BusyStateChanged;
        public event EventHandler RemoveClicked;

        static Random random = new Random();
        static byte[] randomContent = null;
        private static void InitializeRandomContent(long capacity)
        {
            InitializeRandomContentBuffer(capacity);
            random.NextBytes(randomContent);
        }
        private static void InitializeRandomContentBuffer(long capacity)
        {
            if (capacity < 384) capacity = 384;
            if (randomContent == null || randomContent.Length != capacity)
            {
                randomContent = new byte[capacity];
            }
        }


        public string ChosenPath
        {
            get
            {
                var result = string.Empty;
                if (diskDriveComboBox.SelectedIndex == customIndex)
                {
                    result = customLocationComboBox.Text;
                }
                else
                {
                    var di = diskDriveComboBox.SelectedItem as DiskInfo;
                    if (null != di && di.MountPoints.Count > 0)
                    {
                        result = di.MountPoints[0];
                    }
                }
                return result;
            }
        }

        public NoSziestaDriveInfoUC()
        {
            InitializeComponent();

            removeButton.AutoSize = false;
            removeButton.BackgroundImage = Sattelite.Properties.Resources.Delete.ToBitmap();
            removeButton.BackgroundImageLayout = ImageLayout.Stretch;
            removeButton.Text = string.Empty;

            activatedCheckBox.ImageList = new ImageList();
            //activatedCheckBox.ImageList.ImageSize = activatedCheckBox.Size - new Size(4,4);
            //activatedCheckBox.ImageAlign = ContentAlignment.MiddleCenter;
            activatedCheckBox.ImageList.Images.AddRange(new Image[] {
                Sattelite.Properties.Resources.Control_Pause_Blue.ToBitmap()
                ,Sattelite.Properties.Resources.Drive_Error.ToBitmap()
                ,Sattelite.Properties.Resources.Drive_Edit.ToBitmap()
            });

            customLocationPanel_EnabledChanged(null,EventArgs.Empty);
            prevBusy = IsBusy;
            prevActivated = Activated;
            activatedCheckBox_CheckStateChanged(null, EventArgs.Empty);
        }

        public void UpdateRootMountPoints( DiskInfo.Collection NewRootMountPoints )
        {
            RootMountPoints remeberedSelection = null;
            bool remeberedCustomSelection = diskDriveComboBox.SelectedIndex == customIndex;
            if (!remeberedCustomSelection && null != diskDriveComboBox.SelectedItem)
            {
                remeberedSelection = diskDriveComboBox.SelectedItem as RootMountPoints;
            }

            RootMountPoints = NewRootMountPoints;

            this.diskDriveComboBox.Items.Clear();
            this.diskDriveComboBox.Sorted = true;

            var initialSelection = 0;
            foreach (var rmp in RootMountPoints)
            {
                this.diskDriveComboBox.Items.Add(rmp);
            }

            this.diskDriveComboBox.Sorted = false;

            if (null != remeberedSelection)
            {
                for (int i = 0; i < diskDriveComboBox.Items.Count; ++i)
                {
                    var rmp = diskDriveComboBox.Items[i] as RootMountPoints;
                    if (null != rmp && rmp.ToString() == remeberedSelection.ToString())
                    {
                        initialSelection = i;
                        break;
                    }
                }
            }
            customIndex = this.diskDriveComboBox.Items.Add(customLocationComboBoxItem);

            if (remeberedCustomSelection) { initialSelection = customIndex; }

            diskDriveComboBox.SelectedIndex = initialSelection;
        }

        private void customLocationPanel_EnabledChanged(object sender, EventArgs e)
        {
            var isVisible = (diskDriveComboBox.SelectedIndex == customIndex);
            if (isVisible && !customLocationPanel.Visible)
            {
                this.Height += customLocationPanel.Height;
            }
            else if(!isVisible && customLocationPanel.Visible)
            {
                this.Height -= customLocationPanel.Height;
            }
            customLocationPanel.Visible = isVisible;
        }

        private void diskDriveComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            customLocationPanel.Enabled = (diskDriveComboBox.SelectedIndex == customIndex);
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            if (null != RemoveClicked) RemoveClicked(this, e);
        }

        public LastWriteInfo DoDriveWakeup( Guid runTag, int capacity )
        {
            var tempPath = System.IO.Path.Combine(ChosenPath, ".NoSziesta");
            var filePath = System.IO.Path.Combine(tempPath, System.IO.Path.GetRandomFileName()) + ".NoSziestaTemp";
            var result = new LastWriteInfo( filePath );

            try
            {
                RunTag = runTag;

                activatedCheckBox.CheckState = CheckState.Indeterminate;
                Application.DoEvents();
                System.Threading.Thread.Sleep(50);

                if( !System.IO.Directory.Exists(tempPath) ) System.IO.Directory.CreateDirectory(tempPath);

                // delete all existing files after reading <capacity> bytes from them
                var files = System.IO.Directory.GetFiles(tempPath, "*.NoSziestaTemp", System.IO.SearchOption.TopDirectoryOnly);
                int readCapacityLeft = capacity;
                InitializeRandomContentBuffer(capacity);
                foreach (var fileName in files)
                {
                    if (readCapacityLeft > 0)
                    {
                        var readFile = System.IO.File.OpenRead(fileName);
                        readCapacityLeft -= readFile.Read(randomContent, 0, readCapacityLeft);
                        readFile.Close();
                    }
                    System.IO.File.Delete(fileName);
                }

                // write content to file
                var file = System.IO.File.AppendText(result.FilePath);
                file.WriteLine(DateTime.Now.ToLongDateString());
                file.WriteLine(DateTime.Now.ToLongTimeString());
                file.WriteLine("Written by NoSziesta - hard drive sleep depravation software");
                file.WriteLine("Code is available at http://nosziesta.lksz.me");
                file.WriteLine("This is file is already useless, and it is safe to delete it.");
                file.WriteLine("RunTag: {0}", runTag);
                file.WriteLine();
                file.WriteLine();
                file.WriteLine("----- Random filler content -----");
                file.Flush();

                InitializeRandomContent(capacity);
                capacity -= randomContent.Length - (int)file.BaseStream.Position;
                if (capacity > 0)
                {
                    file.BaseStream.Write(randomContent, (int)file.BaseStream.Position, capacity);
                }

                file.Close();
                //System.IO.File.Delete(filePath);
            }
            catch ( Exception x )
            {
                result.Error = x;
            }
            finally
            {
                activatedCheckBox.CheckState = CheckState.Checked;
                Application.DoEvents();
            }
            return result;
        }

        private void activatedCheckBox_CheckStateChanged(object sender, EventArgs e)
        {
            switch (activatedCheckBox.CheckState)
            {
                case CheckState.Unchecked:
                    activatedCheckBox.ImageIndex = 0;
                    break;
                case CheckState.Indeterminate:
                    activatedCheckBox.ImageIndex = 1;
                    break;
                default:
                    activatedCheckBox.ImageIndex = 2;
                    break;
            }

            if (prevBusy != IsBusy && null != BusyStateChanged)
            {
                prevBusy = IsBusy;
                BusyStateChanged(this, EventArgs.Empty);
            }

            if (prevActivated != Activated && null != ActivatedStateChanged)
            {
                prevActivated = Activated;
                ActivatedStateChanged(this, EventArgs.Empty);
            }

            diskDriveComboBox.Enabled = removeButton.Enabled = customLocationPanel.Enabled = !Activated;
        }

		internal void Select(EntryType EntryType, string Value)
		{
			diskDriveComboBox.SelectedIndex = -1;

			if (EntryType == EntryType.WMIDrive)
			{
				foreach (var cbItem in diskDriveComboBox.Items)
				{
					var di = cbItem as DiskInfo;
					if (di == null) continue;

					if (di.WMIDiskDrive.Key == Value)
					{
						diskDriveComboBox.SelectedItem = cbItem;
						break;
					}

					if (diskDriveComboBox.SelectedIndex != -1) break;
				}
			}
			else if (EntryType == EntryType.CustomPath)
			{
				this.diskDriveComboBox.SelectedIndex = customIndex;
				this.customLocationComboBox.Text = Value;
			}
		}

        public class LastWriteInfo
        {
            public DateTime Time { get; private set; }
            public string FilePath { get; private set; }
            public Exception Error { get; internal set; }
            public bool Successful { get { return null == Error; } }

            public LastWriteInfo( string PathName, Exception Error = null ): this( DateTime.Now, PathName, Error ) {}
            public LastWriteInfo( DateTime Time, string PathName, Exception Error ) 
            {
                this.Time = Time;
                this.FilePath = PathName;
                this.Error = Error;
            }
        }
    }
}

[sz]|H:\_Projects\Code\@bitbucket\IdleSharp.NoSziesta		| Sun 04/01/2012 | 16:01:00.89
> @set ErrorLevel=%ErrorLevel%

[sz]|H:\_Projects\Code\@bitbucket\IdleSharp.NoSziesta		| Sun 04/01/2012 | 16:01:00.89
> @rem Restore the original console codepage.

[sz]|H:\_Projects\Code\@bitbucket\IdleSharp.NoSziesta		| Sun 04/01/2012 | 16:01:00.89
> @chcp %cp_oem% > nul < nul
