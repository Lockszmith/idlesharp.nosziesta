
[sz]|H:\_Projects\Code\@bitbucket\IdleSharp.NoSziesta		| Sun 04/01/2012 | 16:00:57.75
> @git.exe %*
﻿namespace IdleSharp.Apps.NoSziesta
{
    partial class NoSziestaDriveInfoUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.activatedCheckBox = new System.Windows.Forms.CheckBox();
            this.diskDriveComboBox = new System.Windows.Forms.ComboBox();
            this.customLocationPanel = new System.Windows.Forms.Panel();
            this.browseLocationButton = new System.Windows.Forms.Button();
            this.customLocationComboBox = new System.Windows.Forms.ComboBox();
            this.removeButton = new System.Windows.Forms.Button();
            this.customLocationPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // activatedCheckBox
            // 
            this.activatedCheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.activatedCheckBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.activatedCheckBox.Location = new System.Drawing.Point(4, 4);
            this.activatedCheckBox.Name = "activatedCheckBox";
            this.activatedCheckBox.Size = new System.Drawing.Size(23, 23);
            this.activatedCheckBox.TabIndex = 0;
            this.activatedCheckBox.UseVisualStyleBackColor = true;
            this.activatedCheckBox.CheckStateChanged += new System.EventHandler(this.activatedCheckBox_CheckStateChanged);
            // 
            // diskDriveComboBox
            // 
            this.diskDriveComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.diskDriveComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.diskDriveComboBox.FormattingEnabled = true;
            this.diskDriveComboBox.Location = new System.Drawing.Point(33, 6);
            this.diskDriveComboBox.Name = "diskDriveComboBox";
            this.diskDriveComboBox.Size = new System.Drawing.Size(283, 21);
            this.diskDriveComboBox.TabIndex = 1;
            this.diskDriveComboBox.SelectedIndexChanged += new System.EventHandler(this.diskDriveComboBox_SelectedIndexChanged);
            // 
            // customLocationPanel
            // 
            this.customLocationPanel.Controls.Add(this.browseLocationButton);
            this.customLocationPanel.Controls.Add(this.customLocationComboBox);
            this.customLocationPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.customLocationPanel.Enabled = false;
            this.customLocationPanel.Location = new System.Drawing.Point(0, 30);
            this.customLocationPanel.Name = "customLocationPanel";
            this.customLocationPanel.Size = new System.Drawing.Size(350, 26);
            this.customLocationPanel.TabIndex = 3;
            this.customLocationPanel.EnabledChanged += new System.EventHandler(this.customLocationPanel_EnabledChanged);
            // 
            // browseLocationButton
            // 
            this.browseLocationButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseLocationButton.AutoSize = true;
            this.browseLocationButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.browseLocationButton.Location = new System.Drawing.Point(322, 0);
            this.browseLocationButton.Name = "browseLocationButton";
            this.browseLocationButton.Size = new System.Drawing.Size(23, 23);
            this.browseLocationButton.TabIndex = 4;
            this.browseLocationButton.Text = "…";
            this.browseLocationButton.UseVisualStyleBackColor = true;
            // 
            // customLocationComboBox
            // 
            this.customLocationComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customLocationComboBox.FormattingEnabled = true;
            this.customLocationComboBox.Location = new System.Drawing.Point(59, 2);
            this.customLocationComboBox.Name = "customLocationComboBox";
            this.customLocationComboBox.Size = new System.Drawing.Size(257, 21);
            this.customLocationComboBox.TabIndex = 3;
            // 
            // removeButton
            // 
            this.removeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.removeButton.AutoSize = true;
            this.removeButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.removeButton.ForeColor = System.Drawing.Color.Red;
            this.removeButton.Location = new System.Drawing.Point(321, 6);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(24, 23);
            this.removeButton.TabIndex = 5;
            this.removeButton.Text = "X";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // NoSziestaDriveInfoUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.customLocationPanel);
            this.Controls.Add(this.diskDriveComboBox);
            this.Controls.Add(this.activatedCheckBox);
            this.Name = "NoSziestaDriveInfoUC";
            this.Size = new System.Drawing.Size(350, 56);
            this.customLocationPanel.ResumeLayout(false);
            this.customLocationPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox activatedCheckBox;
        private System.Windows.Forms.ComboBox diskDriveComboBox;
        private System.Windows.Forms.Panel customLocationPanel;
        private System.Windows.Forms.Button browseLocationButton;
        private System.Windows.Forms.ComboBox customLocationComboBox;
        private System.Windows.Forms.Button removeButton;


    }
}

[sz]|H:\_Projects\Code\@bitbucket\IdleSharp.NoSziesta		| Sun 04/01/2012 | 16:00:57.77
> @set ErrorLevel=%ErrorLevel%

[sz]|H:\_Projects\Code\@bitbucket\IdleSharp.NoSziesta		| Sun 04/01/2012 | 16:00:57.77
> @rem Restore the original console codepage.

[sz]|H:\_Projects\Code\@bitbucket\IdleSharp.NoSziesta		| Sun 04/01/2012 | 16:00:57.77
> @chcp %cp_oem% > nul < nul
