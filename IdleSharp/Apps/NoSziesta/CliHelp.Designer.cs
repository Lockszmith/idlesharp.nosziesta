﻿namespace IdleSharp.Apps.NoSziesta
{
	partial class CliHelp
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CliHelp));
			this.helpTextBox = new System.Windows.Forms.TextBox();
			this.dismissButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// helpTextBox
			// 
			this.helpTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.helpTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.helpTextBox.Location = new System.Drawing.Point(12, 12);
			this.helpTextBox.Multiline = true;
			this.helpTextBox.Name = "helpTextBox";
			this.helpTextBox.ReadOnly = true;
			this.helpTextBox.ShortcutsEnabled = false;
			this.helpTextBox.Size = new System.Drawing.Size(365, 333);
			this.helpTextBox.TabIndex = 0;
			this.helpTextBox.TabStop = false;
			this.helpTextBox.Text = resources.GetString("helpTextBox.Text");
			// 
			// dismissButton
			// 
			this.dismissButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.dismissButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.dismissButton.Location = new System.Drawing.Point(157, 351);
			this.dismissButton.Name = "dismissButton";
			this.dismissButton.Size = new System.Drawing.Size(75, 23);
			this.dismissButton.TabIndex = 1;
			this.dismissButton.Text = "Close";
			this.dismissButton.UseVisualStyleBackColor = true;
			// 
			// CliHelp
			// 
			this.AcceptButton = this.dismissButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.dismissButton;
			this.ClientSize = new System.Drawing.Size(389, 383);
			this.ControlBox = false;
			this.Controls.Add(this.dismissButton);
			this.Controls.Add(this.helpTextBox);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "CliHelp";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "NoSziesta command line help";
			this.TopMost = true;
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox helpTextBox;
		private System.Windows.Forms.Button dismissButton;
	}
}