
[sz]|H:\_Projects\Code\@bitbucket\IdleSharp.NoSziesta		| Sun 04/01/2012 | 16:00:51.59
> @git.exe %*
﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;

namespace IdleSharp.Apps.NoSziesta
{
    public partial class MainForm : Form
    {
        IdleSharp.Sys.WinForms.SplashScreen splash;
        static Random random;
        bool isBusy;
        Guid runTag;
        bool okToClose;
		bool startupComplete;

		// UI Events tracking - to prevent looping
		//bool handlingAddAllUsbClick;
		//bool handlingAutoDetectAndActivateAllUSBDrivesCheckBoxChecked;


        static MainForm()
        {
            random = new Random();
        }

        public MainForm()
        {
            InitializeComponent();

            InitializeForm();
        }
        private void InitializeForm()
        {
			startupComplete = false;

            splash = new IdleSharp.Sys.WinForms.SplashScreen();
            splash.Title = "No Sziesta";
            splash.Version = "Version: " + Application.ProductVersion.ToString();
            splash.Progress = 1;
            splash.Show();

            splash.AdvanceProgress(0, "Initializing...");
            Icon = IdleSharp.Apps.NoSziesta.Sattelite.Properties.Resources.Drive;
            notifyIcon1.Icon = Icon;
            notifyIcon1.Text = Text;

            isBusy = false;
            runTag = Guid.NewGuid();
            okToClose = false;

            IdleSharp.Sys.WinConsole.ConsoleCtrl.Initialize(false);
            IdleSharp.Sys.WinConsole.ConsoleCtrl.SetBufferSize(IdleSharp.Sys.WinConsole.ConsoleCtrl.GetBufferSize().X, 5000);

            splash.AdvanceProgress(20, "Gathering system mount points...");

            splash.AdvanceProgress(60, "Preparing UI elements...");

            Sequential.Items.Clear();
            Sequential.Items.AddRange(MultiDriveMode.All.ToArray<MultiDriveMode>());
            Sequential.SelectedIndex = 0;

            alertPictureBox.Image = Sattelite.Properties.Resources.Error.ToBitmap();

            addNewDriveToolStripButton.Image = Sattelite.Properties.Resources.Drive_Add.ToBitmap();
            addAllUsbToolStripButton.Image = Sattelite.Properties.Resources.Drive_Magnify.ToBitmap();
            clearDrivesToolStripButton.Image = Sattelite.Properties.Resources.Draw_Eraser.ToBitmap();

            busyToolStripStatusLabel.AutoSize = false;
            busyToolStripStatusLabel.Image = IdleSharp.Apps.NoSziesta.Sattelite.Properties.Resources.Drive_Error.ToBitmap();
            busyToolStripStatusLabel.DisplayStyle = ToolStripItemDisplayStyle.Image;
            busyToolStripStatusLabel.ImageScaling = ToolStripItemImageScaling.SizeToFit;
            busyToolStripStatusLabel.Enabled = false;

			splash.AdvanceProgress(10, "Loading startup settings...");

			if (Properties.Settings.Default.StartMinimized)
			{
				this.WindowState = FormWindowState.Minimized;
			}
			autoDetectAndActivateAllUSBDrivesCheckBox_CheckedChanged(null, EventArgs.Empty);

			wmiDriveList.UnhandledException += new EventHandler<UnhandledExceptionEventArgs>(wmiDriveList_UnhandledException);
			wmiDriveList.ActiveStateChanged += new EventHandler(wmiDriveList_ActiveStateChanged);
			wmiDriveList.BusyStateChanged += new EventHandler(wmiDriveList_BusyStateChanged);

			wmiDriveList.RescanActiveDrives();

			var startupIntervalType = (IntervalType)Properties.Settings.Default.IntervalType;
            RefreshEnableState();

			if( IntervalType.Regular == startupIntervalType )
			{
				if (regularIntervalRadioButton.Enabled) regularIntervalRadioButton.Checked = true;
			}
			else if (IntervalType.Random == startupIntervalType)
			{
				if (randomIntervalRadioButton.Enabled) randomIntervalRadioButton.Checked = true;
			}
			else
			{
				disableRadioButton.Checked = true;
			}

			Properties.Settings.Default.SettingChanging += (s, e) => { Properties.Settings.Default.Save(); };
        }

		void wmiDriveList_BusyStateChanged(object sender, EventArgs e)
		{
			busyToolStripStatusLabel.Enabled = wmiDriveList.IsBusy;

		}

		void wmiDriveList_ActiveStateChanged(object sender, EventArgs e)
		{
			var isActive = wmiDriveList.IsActive;

			if (!isActive)
			{
				disableRadioButton.Tag = null;
				randomIntervalRadioButton.Tag = null;
				regularIntervalRadioButton.Tag = null;

				if (disableRadioButton.Checked) disableRadioButton.Tag = true;
				else if (randomIntervalRadioButton.Checked) randomIntervalRadioButton.Tag = true;
				else if (regularIntervalRadioButton.Checked) regularIntervalRadioButton.Tag = true;
			}
			disableRadioButton.Enabled = isActive;
			randomIntervalRadioButton.Enabled = isActive;
			regularIntervalRadioButton.Enabled = isActive;
			if (isActive)
			{
				if (null != disableRadioButton.Tag) disableRadioButton.Checked = true;
				else if (null != randomIntervalRadioButton.Tag) randomIntervalRadioButton.Checked = true;
				else if (null != regularIntervalRadioButton.Tag) regularIntervalRadioButton.Checked = true;

				disableRadioButton.Tag = null;
				randomIntervalRadioButton.Tag = null;
				regularIntervalRadioButton.Tag = null;
			}
		}

		void wmiDriveList_UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			var toolTipText = ((Exception)e.ExceptionObject).Message;
			this.toolTip1.ShowAlways = true;
			this.toolTip1.SetToolTip(this.alertPictureBox, toolTipText);
			alertPictureBox.Visible = !string.IsNullOrWhiteSpace(toolTipText);
		}
        private void RefreshEnableState()
        {
			disableRadioButton.Enabled = wmiDriveList.IsActive;
			randomIntervalRadioButton.Enabled = wmiDriveList.IsActive;
			regularIntervalRadioButton.Enabled = wmiDriveList.IsActive;

			var timerOff = !wmiDriveList.IsActive || disableRadioButton.Checked || !disableRadioButton.Enabled;

            //randomIntervalRadioButton.Enabled = true;
            randomMaxLabel.Enabled = timerOff;
            randomMinLabel.Enabled = timerOff;
            randomMaxNmericUpDown.Enabled = timerOff;
            randomMinNmericUpDown.Enabled = timerOff;
            randomSecondsLabel.Enabled = timerOff;

            //regularIntervalRadioButton.Enabled = true;
            regularIntervalNumericUpDown.Enabled = timerOff;
            regularIntervalSecondsLabel.Enabled = timerOff;

            Sequential.Enabled = timerOff;
            driveModeLabel.Enabled = timerOff;

            writeSizeLabel.Enabled = timerOff;
            writeSizeNumericUpDown.Enabled = timerOff;
            writeSizeKBLabel.Enabled = timerOff;

            addAllUsbToolStripButton.Enabled
                = addNewDriveToolStripButton.Enabled
                = addNewDriveToolStripMenuItem.Enabled
                = clearDrivesToolStripButton.Enabled
                = clearDrivesToolStripMenuItem.Enabled
                = timerOff;

            addNewDriveToolStripButton.ToolTipText = addNewDriveToolStripMenuItem.ToolTipText = timerOff ? "Add new drive to list" : "Edit is disabled while timer is active";
            clearDrivesToolStripButton.ToolTipText = clearDrivesToolStripMenuItem.ToolTipText = timerOff ? "Clear drive list" : "Edit is disabled while timer is active";
            addAllUsbToolStripButton.ToolTipText =  timerOff ? "Automatically load all USB drives to list" : "Edit is disabled while timer is active";

            //systemAutoRestartCheckBox.Enabled = !timerOff;
            startMinimizedCheckBox.Enabled = !timerOff;

            RefreshTimerValues();
        }

        private void busyToolStripStatusLabel_EnabledChanged(object sender, EventArgs e)
        {
            busyToolStripStatusLabel.Visible = busyToolStripStatusLabel.Enabled;
            notifyIcon1.Icon = busyToolStripStatusLabel.Enabled ? Sattelite.Properties.Resources.Drive_Edit : Sattelite.Properties.Resources.Drive;
        }

        private void addNewDriveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            wmiDriveList.AddNewDrive();

            Application.DoEvents();
        }
		private void clearDrives_Click(object sender, EventArgs e)
		{
			autoDetectAndActivateAllUSBDrivesCheckBox.Checked = false;
			wmiDriveList.Clear();
		}

        private void MainForm_Shown(object sender, EventArgs e)
        {
			startupComplete = true;
            splash.ProgressCompleted();
			if (WindowState == FormWindowState.Minimized)
			{
				Hide();
			}
			else
			{
				BringToFront();
			}

        }
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing && !okToClose)
            {
                e.Cancel = true;
                Hide();
            }
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            okToClose = true;
            Close();
        }
        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                Hide();
            }
        }

        private void timerSettings_RadioButtonChanged(object sender, EventArgs e)
        {
            RefreshEnableState();
			if (regularIntervalRadioButton.Enabled && regularIntervalRadioButton.Checked)
			{
				Properties.Settings.Default.IntervalType = IntervalType.Regular.Name;
			}
			else if (randomIntervalRadioButton.Enabled && randomIntervalRadioButton.Checked)
			{
				Properties.Settings.Default.IntervalType = IntervalType.Random.Name;
			}
			else
			{
				Properties.Settings.Default.IntervalType = IntervalType.Disabled.Name;
			}
		}
        private void timerSettings_ValueChanged(object sender, EventArgs e)
        {
            //RefreshTimerValues();
            // TODO: Write Validation code here

        }

        private void RefreshTimerValues()
        {
            timer1.Stop();
            if (disableRadioButton.Checked)
            {
                return;
            }

            var interval = timer1.Interval;
            if (randomIntervalRadioButton.Checked)
            {
                interval = (int)TimeSpan.FromSeconds((int)random.Next((int)randomMinNmericUpDown.Value, (int)randomMaxNmericUpDown.Value)).TotalMilliseconds;
            }
            else
            {
                interval = (int)TimeSpan.FromSeconds((int)regularIntervalNumericUpDown.Value).TotalMilliseconds;
            }

            if (interval != timer1.Interval && interval >= 1000 )
            {
                timer1.Interval = interval;
            }
            if (interval > 0)
            {
                timer1.Start();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (randomIntervalRadioButton.Checked) RefreshTimerValues();
            if (isBusy) return;
            isBusy = true;
            var tagApplied = false;
            var tagAppliedLast = false;
            try
            {
                foreach (var szDrive in wmiDriveList.AllDrives() )
                {
                    if (szDrive.Activated && !string.IsNullOrWhiteSpace(szDrive.ChosenPath))
                    {
                        tagAppliedLast = false;
                        if (tagApplied && Sequential.SelectedItem == MultiDriveMode.OneDrivePerInterval)
                        {
                            continue;
                        }

                        if (!szDrive.RunTag.Equals(runTag))
                        {
                            tagApplied = true;
                            tagAppliedLast = false;
                        }
                        szDrive.DoDriveWakeup(runTag, 1024 * (int)writeSizeNumericUpDown.Value);
                    }
                }
            }
            finally
            {
                if (!tagApplied || tagAppliedLast)
                {
                    runTag = Guid.NewGuid();
                }
                isBusy = false;

                busyToolStripStatusLabel.Enabled = false;
                Application.DoEvents();
            }
        }

        private void showNotifyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Show();
            if (WindowState == FormWindowState.Minimized)
            {
                Bounds = RestoreBounds;
                WindowState = FormWindowState.Normal;
            }
            BringToFront();
        }

        private void addAllUsb_Click(object sender, EventArgs e)
        {
			if (wmiDriveList.DriveCount == 0 && !autoDetectAndActivateAllUSBDrivesCheckBox.Checked)
			{
				autoDetectAndActivateAllUSBDrivesCheckBox.Checked = true;
			}
			else
			{
				wmiDriveList.AutoAddAllUsbDrives();
			}
        }

		private void autoDetectAndActivateAllUSBDrivesCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			//if (handlingAutoDetectAndActivateAllUSBDrivesCheckBoxChecked) return;
			//try
			//{
			//    handlingAutoDetectAndActivateAllUSBDrivesCheckBoxChecked = true;

				if (wmiDriveList.DriveCount > 0 && autoDetectAndActivateAllUSBDrivesCheckBox.Checked)
				{
					if (System.Windows.Forms.DialogResult.OK != MessageBox.Show(
							string.Format("This will clear all currently entered drives from the list{0}Are you sure you want to continue?", Environment.NewLine)
							, "Caution!"
							, MessageBoxButtons.OKCancel))
					{
						autoDetectAndActivateAllUSBDrivesCheckBox.Checked = false;
						return;
					}
				}
				wmiDriveList.Enabled = !autoDetectAndActivateAllUSBDrivesCheckBox.Checked;

				//if (handlingAddAllUsbClick) return;

				if (autoDetectAndActivateAllUSBDrivesCheckBox.Checked)
				{
					wmiDriveList.AutoAddAllUsbDrives(true, startupComplete);
				}
			//}
			//finally
			//{
			//    handlingAutoDetectAndActivateAllUSBDrivesCheckBoxChecked = false;
			//}
		}

        //private void notifyIcon1_Click(object sender, EventArgs e)
        //{
        //    Show();
        //    if (WindowState == FormWindowState.Minimized)
        //    {
        //        Bounds = RestoreBounds;
        //        WindowState = FormWindowState.Normal;
        //    }
        //    BringToFront();
        //}
    }

    public class MultiDriveMode: IdleSharp.Sys.EnumBase<MultiDriveMode, string>
    {
        public static readonly MultiDriveMode Sequential = new MultiDriveMode("Sequential", "Sequential");
        public static readonly MultiDriveMode OneDrivePerInterval = new MultiDriveMode("One Drive Timer-tick Interval", "OneDrivePerInterval");
        public static readonly MultiDriveMode Parallel = new MultiDriveMode("Parallel", "Parallel");

        public new static IEnumerable<MultiDriveMode> All { get { return IdleSharp.Sys.EnumBase<MultiDriveMode, string>.All; } }

        private MultiDriveMode(string Name, string Value) : base(Name, Value) { }
    }
}

[sz]|H:\_Projects\Code\@bitbucket\IdleSharp.NoSziesta		| Sun 04/01/2012 | 16:00:51.61
> @set ErrorLevel=%ErrorLevel%

[sz]|H:\_Projects\Code\@bitbucket\IdleSharp.NoSziesta		| Sun 04/01/2012 | 16:00:51.61
> @rem Restore the original console codepage.

[sz]|H:\_Projects\Code\@bitbucket\IdleSharp.NoSziesta		| Sun 04/01/2012 | 16:00:51.61
> @chcp %cp_oem% > nul < nul
