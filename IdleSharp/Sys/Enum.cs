﻿//
// Copyright (C) 2011 Gal Szkolnik aka Lockszmith (http://code.lockszmith.com)
// Originally posted at http://code.lockszmith.com/a-different-type-of-enums
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//   - The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
using System;
using System.Collections.Generic;
using System.Linq; // for the .AsEnumerable() method call

namespace IdleSharp.Sys
{
    // Code has been made public at http://lksz.me/typeSafeEnums

    // E is the derived type-safe-enum class
    // - this allows all static members to be truly unique to the specific
    //   derived class
    public interface IEnumBase<E, T> where E: IEnumBase<E, T>
    {
        T Value { get; }
        string Name { get; }
    }

    public abstract class EnumBase<E, T> : IEnumBase<EnumBase<E, T>, T> where E: EnumBase<E, T>
    {
        #region Instance code
        public T Value { get; private set; }
        public string Name { get; private set; }

        protected EnumBase(string Name, T EnumValue)
        {
            Value = EnumValue;
            this.Name = Name;
            mapping.Add(Name, this);
        }

        public override string ToString() { return Name; }
        #endregion

        #region Static tools
        static private readonly Dictionary<string, EnumBase<E, T>> mapping;
        static EnumBase() { mapping = new Dictionary<string, EnumBase<E, T>>(); }
        protected static E Parse(string name)
        {
            EnumBase<E, T> result;
            if (mapping.TryGetValue(name, out result))
            {
                return (E)result;
            }

            throw new InvalidCastException();
        }
        // This is protected to force the child class to expose it's own static
        // method.
        // By recreating this static method at the derived class, static
        // initialization will be explicit, promising the mapping dictionary
        // will never be empty when this method is called.
        protected static IEnumerable<E> All { get { return mapping.Values.AsEnumerable().Cast<E>(); } }
        #endregion
    }

    public class EnumBase<E> : IEnumBase<E, string> where E : EnumBase<E>
    {
        #region Instance code
        public string Value { get { return Name; } }
        public string Name { get; private set; }

        protected EnumBase(string Name)
        {
            this.Name = Name;
            mapping[Name] = this;
        }

        public override string ToString() { return Name; }
        #endregion

        #region Static tools
        static private readonly Dictionary<string, EnumBase<E>> mapping;
        static EnumBase() { mapping = new Dictionary<string, EnumBase<E>>(); }
        protected static bool TryGet( string name, out E value )
        {
            EnumBase<E> result;
            if (mapping.TryGetValue(name, out result))
            {
                value = (E)result;
                return true;
            }
            value = null;
            return false;
        }
        protected static E Parse(string name)
        {
            EnumBase<E> result;
            if (mapping.TryGetValue(name, out result))
            {
                return (E)result;
            }

            throw new InvalidCastException();
        }
        // This is protected to force the child class to expose it's own static
        // method.
        // By recreating this static method at the derived class, static
        // initialization will be explicit, promising the mapping dictionary
        // will never be empty when this method is called.
        protected static IEnumerable<E> All { get { return mapping.Values.AsEnumerable().Cast<E>(); } }
        public static implicit operator string(EnumBase<E> Enum) { return Enum.Value; }
        #endregion
    }


    public sealed class YesNoEnum : EnumBase<YesNoEnum, int>
    {
        public static readonly YesNoEnum Yes = new YesNoEnum(0, "Yes");
        public static readonly YesNoEnum No = new YesNoEnum(1, "No");

        private YesNoEnum(int value, String name) : base(name, value) { }
        public new static IEnumerable<YesNoEnum> All { get { return EnumBase<YesNoEnum, int>.All; } }
        public static explicit operator YesNoEnum(string str) { return Parse(str); }
    }
}
