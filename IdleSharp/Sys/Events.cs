﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IdleSharp.Sys
{
	public static class Events
	{
		public static void TriggerEvent(EventHandler handler)
		{
			TriggerEvent<EventArgs>(handler, (object)null, EventArgs.Empty);
		}
		public static void TriggerEvent(EventHandler handler, object sender)
		{
			TriggerEvent<EventArgs>(handler, sender, EventArgs.Empty);
		}
		public static void TriggerEvent<TEventArgs>(EventHandler handler, Func<TEventArgs> getEventArgs) where TEventArgs: EventArgs
		{
			TriggerEvent<TEventArgs>(handler, (object)null, getEventArgs);
		}
		public static void TriggerEvent<TEventArgs>(EventHandler handler, TEventArgs e) where TEventArgs : EventArgs
		{
			TriggerEvent<TEventArgs>(handler, (object)null, e);
		}
		public static void TriggerEvent<TEventArgs>(EventHandler handler, object sender, Func<TEventArgs> getEventArgs) where TEventArgs : EventArgs
		{
			if (null != handler)
			{
				var e = getEventArgs();
				handler(sender, e);
			}
		}
		public static void TriggerEvent<TEventArgs>(EventHandler handler, object sender, TEventArgs e) where TEventArgs : EventArgs
		{
			if (null != handler)
			{
				handler(sender, e);
			}
		}
		public static void TriggerEvent<TEventArgs>(EventHandler<TEventArgs> handler) where TEventArgs : EventArgs
		{
			TriggerEvent<TEventArgs>(handler, (object)null, (TEventArgs)EventArgs.Empty);
		}
		public static void TriggerEvent<TEventArgs>(EventHandler<TEventArgs> handler, object sender) where TEventArgs : EventArgs
		{
			TriggerEvent<TEventArgs>(handler, sender, (TEventArgs)EventArgs.Empty);
		}
		public static void TriggerEvent<TEventArgs>(EventHandler<TEventArgs> handler, Func<TEventArgs> getEventArgs) where TEventArgs : EventArgs
		{
			TriggerEvent<TEventArgs>(handler, (object)null, getEventArgs);
		}
		public static void TriggerEvent<TEventArgs>(EventHandler<TEventArgs> handler, TEventArgs e) where TEventArgs : EventArgs
		{
			TriggerEvent<TEventArgs>(handler, (object)null, e);
		}
		public static void TriggerEvent<TEventArgs>(EventHandler<TEventArgs> handler, object sender, Func<TEventArgs> getEventArgs) where TEventArgs : EventArgs
		{
			if (null != handler)
			{
				var e = getEventArgs();
				handler(sender, e);
			}
		}
		public static void TriggerEvent<TEventArgs>(EventHandler<TEventArgs> handler, object sender, TEventArgs e) where TEventArgs : EventArgs
		{
			if (null != handler)
			{
				handler(sender, e);
			}
		}

		// The same methods above as extension methods
		public static void TriggerEvent(this object sender, EventHandler handler)
		{
			TriggerEvent(handler, sender);
		}
		public static void TriggerEvent<TEventArgs>(this object sender, EventHandler handler, TEventArgs e) where TEventArgs : EventArgs
		{
			TriggerEvent<TEventArgs>(handler, sender, e);
		}
		public static void TriggerEvent<TEventArgs>(this object sender, EventHandler handler, Func<TEventArgs> getEventArgs) where TEventArgs : EventArgs
		{
			TriggerEvent<TEventArgs>(handler, sender, getEventArgs);
		}
		public static void TriggerEvent<TEventArgs>(this object sender, EventHandler<TEventArgs> handler) where TEventArgs : EventArgs
		{
			TriggerEvent<TEventArgs>(handler, sender);
		}
		public static void TriggerEvent<TEventArgs>(this object sender, EventHandler<TEventArgs> handler, TEventArgs e) where TEventArgs : EventArgs
		{
			TriggerEvent<TEventArgs>(handler, sender, e);
		}
		public static void TriggerEvent<TEventArgs>(this object sender, EventHandler<TEventArgs> handler, Func<TEventArgs> getEventArgs) where TEventArgs : EventArgs
		{
			TriggerEvent<TEventArgs>(handler, sender, getEventArgs);
		}
	}
}
