﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace IdleSharp.Sys.WinForms
{
    public partial class SplashScreen : Form
    {
        public TextBox ProgressTextBox { get { return progressDescriptionTextBox; } }
        public string ProgressText { get { return progressDescriptionTextBox.Text; } set { progressDescriptionTextBox.Text = value; } }
        public Label TitleLabel { get { return titleLabel; } }
        public string Title { get { return titleLabel.Text; } set { titleLabel.Text = value; } }
        public string Version { get { return versionLabel.Text; } set { versionLabel.Text = value; } }

        public int Progress { get { return progressBar.Value; } set { progressBar.Value = value; } }

        public SplashScreen()
        {
            InitializeComponent();
        }

        public void AdvanceProgress( int progressStep, string progressText = null )
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action<int, string>(AdvanceProgress), progressStep, progressText);
                return;
            }

            if (progressStep < 0 && (Progress + progressStep) < 0)
            {
                Progress = 0;
            }
            if (Progress + progressStep > progressBar.Maximum)
            {
                Progress = progressBar.Maximum;
            }
            else
            {
                Progress += progressStep;
            }

            if (!string.IsNullOrEmpty(progressText))
            {
                progressDescriptionTextBox.Select(progressDescriptionTextBox.Text.Length, 0);
                if (progressDescriptionTextBox.SelectionStart != 0)
                {
                    progressDescriptionTextBox.SelectedText = Environment.NewLine;
                    progressDescriptionTextBox.Select(progressDescriptionTextBox.Text.Length, 0);
                }
                progressDescriptionTextBox.SelectedText = progressText;
            }
            Application.DoEvents();
        }

        private void SplashScreen_Shown(object sender, EventArgs e)
        {
            Application.DoEvents();
        }

        public void ProgressCompleted(string message = "Done.")
        {
            AdvanceProgress(progressBar.Maximum - Progress, message);
            var t = new Timer();
            t.Interval = 3000;
            t.Tick += new EventHandler((s, e) => { ((Timer)s).Stop(); Close(); });
            t.Start();
        }
    }
}
