﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Management;
using System.Text;

namespace IdleSharp.Apps.NoSziesta.WMIClasses
{
    public class Volume : WMIClass
    {
        public class PropertyName: IdleSharp.Sys.EnumBase<PropertyName>
        {
            #region Type-Safe-Enum pattern code
            private PropertyName(String name) : base(name) { }
            public new static IEnumerable<PropertyName> All { get { return Sys.EnumBase<PropertyName>.All; } }
            public static explicit operator PropertyName(string str) { return Parse(str); }
            #endregion

            public new static readonly PropertyName Name = new PropertyName("Name");
            public static readonly PropertyName DeviceID = new PropertyName("DeviceID");
            public static readonly PropertyName FileSystem = new PropertyName("FileSystem");
            public static readonly PropertyName DriveType = new PropertyName("DriveType");
            public static readonly PropertyName SerialNumber = new PropertyName("SerialNumber");
            public static readonly PropertyName Label = new PropertyName("Label");
            #region Old Const implementation
            //public static class Win32_Volume
            //{
            //    public const string _ = "Win32_Volume";

            //    public const string Name = Basic.Name;
            //    public const string DeviceID = "DeviceID";
            //    public const string FileSystem = "FileSystem";
            //    public const string DriveType = "DriveType";
            //    public static readonly string[] DriveTypeNames = new string[] {
            //              "Unknown"
            //            , "No Root Directory"
            //            , "Removable Disk"
            //            , "Local Disk"
            //            , "Network Drive"
            //            , "Compact Disc"
            //            , "RAM Disk"
            //        };
            //    public const string SerialNumber = "SerialNumber";
            //    public const string Label = "Label";
            //    //[00]: "Automount"
            //    //[01]: "BlockSize"
            //    //[02]: "BootVolume"
            //    //[03]: "Capacity"
            //    //[04]: "Caption"
            //    //[05]: "DeviceID"
            //    //[06]: "DirtyBitSet"
            //    //[07]: "DriveType"
            //    //[08]: "FileSystem"
            //    //[09]: "FreeSpace"
            //    //[10]: "Label"
            //    //[11]: "MaximumFileNameLength"
            //    //[12]: "Name"
            //    //[13]: "PageFilePresent"
            //    //[14]: "SerialNumber"
            //    //[15]: "SupportsDiskQuotas"
            //    //[16]: "SupportsFileBasedCompression"
            //    //[17]: "SystemName"
            //    //[18]: "SystemVolume"
            //}
            #endregion
        }
        public string this[PropertyName Key] { get { return base.Properties[Key]; } }
        public override WMIClass.ClassName WMIClassName { get { return WMIClass.ClassName.Win32_Volume; } }
        protected override string KeyName { get { return PropertyName.DeviceID; } }

        public Volume() : base(null) { }
        public Volume(System.Management.ManagementObject wmiObject) : base(wmiObject) { }

        public static Collection<Volume> GetVolumes() { return new Factory<Volume>().GetInstances(); }
        internal static WMIClass.Collection<Volume> GetVolumeFor(MountPoint MountPoint)
        {
            return new Factory<Volume>().GetInstancesRelatedTo(MountPoint);
        }
    }
}
