﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Management;

namespace IdleSharp.Apps.NoSziesta.WMIClasses
{
    public class LogicalDisk: WMIClass
    {
        public class PropertyName : IdleSharp.Sys.EnumBase<PropertyName>
        {
            #region Type-Safe-Enum pattern code
            private PropertyName(String name) : base(name) { }
            public new static IEnumerable<PropertyName> All { get { return Sys.EnumBase<PropertyName>.All; } }
            public static explicit operator PropertyName(string str) { return Parse(str); }
            #endregion

            public new static readonly PropertyName Name = new PropertyName("Name");
        }
        public string this[PropertyName Key] { get { return base.Properties[Key]; } }
        public override WMIClass.ClassName WMIClassName { get { return WMIClass.ClassName.Win32_LogicalDisk; } }
        protected override string KeyName { get { return PropertyName.Name; } }

        public LogicalDisk() : base(null) { }
        public LogicalDisk(System.Management.ManagementObject wmiObject) : base(wmiObject) { }

        public static Collection<LogicalDisk> GetDirectories() { return new Factory<LogicalDisk>().GetInstances(); }
        public static Collection<LogicalDisk> GetDirectoriesFor(Volume Volume)
        {
            return new Factory<LogicalDisk>().GetInstancesRelatedTo(Volume);
        }
    }
}
