﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IdleSharp.Apps.NoSziesta.WMIClasses
{
    public class DiskDrive : WMIClass
    {
        public class PropertyName : IdleSharp.Sys.EnumBase<PropertyName>
        {
            #region Type-Safe-Enum pattern code
            private PropertyName(String name) : base(name) { }
            public new static IEnumerable<PropertyName> All { get { return Sys.EnumBase<PropertyName>.All; } }
            public static explicit operator PropertyName(string str) { return Parse(str); }
            #endregion

            public new static readonly PropertyName Name = new PropertyName("Name");
            public static readonly PropertyName MediaType = new PropertyName("MediaType");
            public static readonly PropertyName InterfaceType = new PropertyName("InterfaceType");
        }
        public string this[PropertyName Key] { get { return base.Properties[Key]; } }
        public override WMIClass.ClassName WMIClassName { get { return WMIClass.ClassName.Win32_DiskDrive; } }

        public DiskDrive() : base(null) { }
        public DiskDrive(System.Management.ManagementObject wmiObject) : base(wmiObject) { }

        public static Collection<DiskDrive> GetDiskDrives() { return new Factory<DiskDrive>().GetInstances(); }
        internal static WMIClass.Collection<DiskDrive> GetDiskDrivesFor(MountPoint MountPoint)
        {
            return new Factory<DiskDrive>().GetInstancesRelatedTo(MountPoint);
        }

    }
}
