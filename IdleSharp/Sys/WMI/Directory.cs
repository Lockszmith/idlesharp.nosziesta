﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Management;

namespace IdleSharp.Apps.NoSziesta.WMIClasses
{
    public class Directory: WMIClass
    {
        public class PropertyName : IdleSharp.Sys.EnumBase<PropertyName>
        {
            #region Type-Safe-Enum pattern code
            private PropertyName(String name) : base(name) { }
            public new static IEnumerable<PropertyName> All { get { return Sys.EnumBase<PropertyName>.All; } }
            public static explicit operator PropertyName(string str) { return Parse(str); }
            #endregion

            public static readonly PropertyName Directory = new PropertyName("Directory");
        }
        public string this[PropertyName Key] { get { return base.Properties[Key]; } }
        public override WMIClass.ClassName WMIClassName { get { return WMIClass.ClassName.Win32_Directory; } }
        protected override string KeyName { get { return PropertyName.Directory; } }

        public Directory() : base(null) { }
        public Directory(System.Management.ManagementObject wmiObject) : base(wmiObject) { }

        public static WMIClass.Collection<Directory> GetDirectories() { return new WMIClass.Factory<Directory>().GetInstances(); }
        public static WMIClass.Collection<Directory> GetDirectoriesFor(Volume Volume)
        {
            return new WMIClass.Factory<Directory>().GetInstancesRelatedTo(Volume);
        }
    }
}
