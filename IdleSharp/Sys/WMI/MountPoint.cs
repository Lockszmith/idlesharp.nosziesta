﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;

namespace IdleSharp.Apps.NoSziesta.WMIClasses
{
    public class MountPoint : WMIClass
    {
        public class PropertyName : IdleSharp.Sys.EnumBase<PropertyName>
        {
            #region Type-Safe-Enum pattern code
            private PropertyName(String name) : base(name) { }
            public new static IEnumerable<PropertyName> All { get { return Sys.EnumBase<PropertyName>.All; } }
            public static explicit operator PropertyName(string str) { return Parse(str); }
            #endregion

            public static readonly PropertyName Directory = new PropertyName("Directory");
            public static readonly PropertyName Volume = new PropertyName("Volume");
        }
        public string this[PropertyName Key] { get { return base.Properties[Key]; } }
        public override WMIClass.ClassName WMIClassName { get { return WMIClass.ClassName.Win32_MountPoint; } }
        protected override string KeyName { get { return PropertyName.Directory; } }

        public MountPoint() : base(null) { }
        public MountPoint(System.Management.ManagementObject wmiObject) : base(wmiObject) { }

        internal static MountPoint.Collection<MountPoint> GetMountPointFor(Volume Volume)
        {
            return new MountPoint.Factory<MountPoint>().GetInstancesRelatedTo(Volume);
        }
    }
}
