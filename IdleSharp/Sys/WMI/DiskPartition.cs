﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IdleSharp.Apps.NoSziesta.WMIClasses
{
    public class DiskPartition : WMIClass
    {
        public class PropertyName : IdleSharp.Sys.EnumBase<PropertyName>
        {
            #region Type-Safe-Enum pattern code
            private PropertyName(String name) : base(name) { }
            public new static IEnumerable<PropertyName> All { get { return Sys.EnumBase<PropertyName>.All; } }
            public static explicit operator PropertyName(string str) { return Parse(str); }
            #endregion

            public new static readonly PropertyName Name = new PropertyName("Name");
        }
        public string this[PropertyName Key] { get { return base.Properties[Key]; } }
        public override WMIClass.ClassName WMIClassName { get { return WMIClass.ClassName.Win32_DiskPartition; } }

        public DiskPartition() : base(null) { }
        public DiskPartition(System.Management.ManagementObject wmiObject) : base(wmiObject) { }

        public static WMIClass.Collection<DiskPartition> GetDiskPartitions() { return new WMIClass.Factory<DiskPartition>().GetInstances(); }
        public static WMIClass.Collection<DiskPartition> GetDiskPartitionsFor(Volume Volume)
        {
            return new WMIClass.Factory<DiskPartition>().GetInstancesRelatedTo(Volume);
        }
    }
}
