﻿using System;
//using System.Collections.Generic;
using System.Management;
using System.Collections.Specialized;
using System.Collections.ObjectModel;

namespace IdleSharp.Apps.NoSziesta
{
    #region WMI old namespace
    namespace WMIold
    {
        public static class Basic
        {
            public const string Name = "Name";
        }
        //public static class Win32_LogicalDisk
        //{
        //    public static string _For(string logicalDiskKey) { return string.Format("{0}='{1}'", WMI.Win32_LogicalDisk._, logicalDiskKey); }
        //}
        public static class Win32_MountPoint
        {
            public const string Directory = "Directory";
        }
        public static class Scope
        {
            public static readonly ManagementScope Root = new ManagementScope("\\\\.\\ROOT\\cimv2");
            public static readonly ManagementScope Default = Root;
        }
        public static class Condition
        {
            public static string Equal(string Field, string Value) { return string.Format("{0}='{1}'", Field, Value); }
            public static string Equal(string Class, string Field, string Value) { return string.Format("{0}.{1}='{2}'", Class, Field, Value); }
        }
    }
    #endregion

    public abstract class WMIClass: IDisposable
    {
        #region Class Names
        public class ClassName : IdleSharp.Sys.EnumBase<ClassName>
        {
            public static readonly ClassName Win32_Volume = GetClassName("Win32_Volume");
            public static readonly ClassName Win32_LogicalDisk = GetClassName("Win32_LogicalDisk");
            //public static readonly ClassName Win32_LocalDisk = GetClassName("Win32_LocalDisk");
            public static readonly ClassName Win32_DiskDrive = GetClassName("Win32_DiskDrive");
            public static readonly ClassName Win32_MountPoint = GetClassName("Win32_MountPoint");
            public static readonly ClassName Win32_Directory = GetClassName("Win32_Directory");
            public static readonly ClassName Win32_DiskPartition = GetClassName("Win32_DiskPartition");
            //public static readonly ClassName _ = new ClassName("_");

            private ClassName(string Name) : base(Name) { }
            internal static ClassName GetClassName( string Name )
            {
                ClassName result;
                if (IdleSharp.Sys.EnumBase<ClassName>.TryGet(Name, out result))
                    return result;
                return new ClassName(Name);
            }
        }
        #endregion
        #region Internal static helper methods
        internal static WqlObjectQuery WMIQuery(WMIClass.ClassName ClassName, string PropertyName, string ValueString)
        {
            return WMIQuery(ClassName, PropertyName, "=", ValueString);
        }
        internal static WqlObjectQuery WMIQuery(WMIClass.ClassName ClassName, string PropertyName, string Operator, string ValueString)
        {
            var sb = new System.Text.StringBuilder(PropertyName.Length + Operator.Length + ValueString.Length + 2); // 2 = 2 quotes
            sb.Append(PropertyName);
            sb.Append(Operator);
            sb.Append('"');
            sb.Append(ValueString);
            sb.Append('"');
            return new SelectQuery(ClassName, sb.ToString());
        }

        //internal static WqlObjectQuery WMIQuery(WMI.ClassName ClassName, string ValueString)
        //{
        //    return WMIQuery(ClassName, string.Empty, "=", ValueString);
        //}
        #endregion

        protected virtual string KeyName { get { return "Name"; } }
        public abstract WMIClass.ClassName WMIClassName { get; }
        //protected string ID { get { return new Mana WMIQuery(WMIClassName, Key); } }
        public ManagementObject wmiObject { get; private set; }

        internal virtual void Init(ManagementObject wmiObject)
        {
            this.wmiObject = wmiObject;
        }
        internal WMIClass(ManagementObject wmiObject) { Init( wmiObject ); Properties = new NameValueCollection(); }

        public string Key { get { return Properties[KeyName];  } }
        public NameValueCollection Properties { get; private set; }
        protected string this[string Key] { get { return Properties[Key]; } }

        public class Collection<wmiC> : KeyedCollection<string, wmiC> where wmiC : WMIClass
        {
            protected override string GetKeyForItem(wmiC item)
            {
                return item.Key;
            }
        }
        public class Factory<wmiC> where wmiC : WMIClass, new()
        {
            public Factory() { _wimClassName = new wmiC().WMIClassName;  }
            internal Factory(string GenericClassName) { _wimClassName = ClassName.GetClassName(GenericClassName);  }
            internal Factory(ClassName GenericClassName) { _wimClassName = GenericClassName; }
            private WMIClass.ClassName _wimClassName;
            protected WMIClass.ClassName WMIClassName { get { return _wimClassName; } }

            public WMIClass.Collection<wmiC> GetInstances()
            {
                using (var wmiClass = new ManagementClass(WMIClassName))
                {
                    using (var wmiInstances = wmiClass.GetInstances())
                    {
                        return CreateFromWmiInstances(wmiInstances);
                    }
                }
            }
            public WMIClass.Collection<wmiC> GetInstancesWhere(WMIClass Related, string PropertyName)
            {
                using (var wmiSearcher = new ManagementObjectSearcher( WMIQuery(  WMIClassName, PropertyName , Related[PropertyName])))
                {
                    using (var wmiInstances = wmiSearcher.Get())
                    {
                        return CreateFromWmiInstances(wmiInstances);
                    }
                }
            }
            //public wmiC GetInstanceWhere(string PropertyName, string PropertyValue)
            //{
            //    var wmiInstance = new ManagementObject(WMIQuery(WMIClassName, PropertyName, "=", PropertyValue));
            //    if( null == wmiInstance ) return null;
            //    return CreateFromWmiInstance(wmiInstance);
            //}
            public WMIClass.Collection<wmiC> GetInstancesFromRelationshipTo(WMIClass Related)
            {
                if (null == Related) return new WMIClass.Collection<wmiC>();

                // find the mountpoints of this volume
                using (var wmiInstances = Related.wmiObject.GetRelationships(WMIClassName))
                {
                    return CreateFromWmiInstances(wmiInstances);
                }
            }
            public WMIClass.Collection<wmiC> GetInstancesRelatedTo(WMIClass Related)
            {
                if (null == Related) return new WMIClass.Collection<wmiC>();

                //using (var wmiClass = new ManagementClass(WMIClassName))
                //{
                //    using (var wmiInstances = wmiClass.GetRelated(WMI.WMIQuery(WMIClassName, Related.KeyName, "=", Related.Key)))
                //    {
                //        return CreateFromWmiInstances(wmiInstances);
                //    }
                //}

                using (var wmiInstances = Related.wmiObject.GetRelated(WMIClassName))
                {
                    return CreateFromWmiInstances(wmiInstances);
                }
            }

            private wmiC CreateFromWmiInstance(ManagementObject wmiObject)
            {
                var newObject = new wmiC();
                newObject.Init(wmiObject);
                // for each non-null property on the Volume, add it to our NameValueCollection

                foreach (var p in wmiObject.Properties)
                {
                    if (p.Value == null)
                        continue;
                    var v = p.Value.ToString();
                    var i = v.IndexOf("=\"");
                    if (i > 0)
                    {
                        v = v.Substring(i + 2, v.Length - i - 3);
                    }
                    newObject.Properties.Add(p.Name, v);
                }

                return newObject;
            }
            private WMIClass.Collection<wmiC> CreateFromWmiInstances(ManagementObjectCollection wmiObjects)
            {
                var result = new WMIClass.Collection<wmiC>();

                if( wmiObjects.Count == 0 ) return result;

                foreach (ManagementObject wmiObject in wmiObjects)
                {
                    result.Add(CreateFromWmiInstance(wmiObject));
                }
                return result;
            }
        }

        public class Generic : WMIClass
        {
            public new string this[string Key] { get { return base.Properties[Key]; } }
            private WMIClass.ClassName _wmiClassName;
            public override WMIClass.ClassName WMIClassName { get { return _wmiClassName; } }
            private string _keyName;
            protected override string KeyName { get { return _keyName; } }

            internal override void Init(ManagementObject wmiObject)
            {
                base.Init(wmiObject);
                if (null == wmiObject) return;
                _wmiClassName = ClassName.GetClassName(wmiObject.ClassPath.ClassName);
                _keyName = "Name";
            }
            public Generic() : base(null) { }
            public Generic(string ClassName, string KeyName)
                : base(null)
            {
                _wmiClassName = WMIClass.ClassName.GetClassName(ClassName);
                _keyName = KeyName;
            }
            public Generic(System.Management.ManagementObject wmiObject) : base(wmiObject) { }

            public static Collection<Generic> GetInstances(string ClassName) { return new Factory<Generic>(ClassName).GetInstances(); }
            public static Collection<Generic> GetInstances(ClassName ClassName) { return new Factory<Generic>(ClassName).GetInstances(); }
        }

        void IDisposable.Dispose()
        {
            if (null != wmiObject)
            {
                wmiObject.Dispose();
            }
        }
    }

}
